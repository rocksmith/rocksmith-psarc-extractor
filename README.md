# rocksmith-psarc-extractor

TypeScript .psarc unpacker for Rocksmith DLC (only song manifest as of now)

# Usage

```
rocksmith_psarc_extractor.exe <input-file>
```

# Build Requirements

Install Node.js (https://nodejs.org/en/)

Install the typescript compiler and pkg
```
npm install -g typescript
npm install -g pkg
```

# Build Instructions

```
build.bat
```